﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Tolltech.MuserUI.Controllers
{
    [AllowAnonymous]
    [Route("kontur")]
    public class KonturController : BaseController
    {
        [HttpGet("paymentorder")]
        public IActionResult PaymentOrderForm()
        {
            return View();
        }

        [HttpPost("paymentorder")]
        public string GetPaymentOrder(string paymentOrderText)
        {
            return paymentOrderText;
        }
    }
}